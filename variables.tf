variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "a-zone" {}
variable "env_prefix" {}
variable "my-ip" {}
variable "public_key_location" {}
variable "private_key_location" {}
