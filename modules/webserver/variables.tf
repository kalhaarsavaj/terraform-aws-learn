variable "vpc_id" {}
variable "my-ip" {}
variable "env_prefix" {}
variable "public_key_location" {}
variable "instance_type" {}
variable "subnet_id" {}
variable "a-zone" {}
variable "private_key_location" {}